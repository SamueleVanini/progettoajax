<?php

class Db
{
    private $servername = "localhost";
    private $username = "root";
    private $password = "";
    private $dbname = "ajax";
	// get the q parameter from URL
    private $q;
    // stringa per i riscontri
    private $hint = "";
    
    public function __construct() {
        $this->q = $_REQUEST["q"];
    }
    
    // Metodo per la creazione di una connessione
    public function connessione() 
    {
        // Creazione conessione
        $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        // Controllo connessione
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } else {
            return $conn;
        }
    }
    
    // Metodo per l'esecuzione della query
    public function query($conn) 
    {
        // creazione query
        $sql = "SELECT Name 
				FROM names
				WHERE Name like '".$this->q."%'
				LIMIT 100";
        // esecuzione query
        $result = $conn->query($sql);
        return $result;
    }
    
    // Metodo per la chiusura della connessione verso il database
    public function closeConnection() 
    {
        $conn->close();
    }
    
    public function stampaRisultati($result)
    {
		 while($row = $result->fetch_assoc()) 
		 {
			 $this->hint .= $row['Name']." ; ";
		 }
		 echo $this->hint === "" ? "no suggestion" : $this->hint;
	}
    
}

$db = new Db();
$risultati = $db->query($db->connessione());
$db->stampaRisultati($risultati);
?>
