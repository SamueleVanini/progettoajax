# ProgettoAjax

### Descrizione

Questo progetto basato sulla tecnica di programmazione Ajax da una introduzione all'argomento, illustra il codice usato e permette di avere dei suggerimenti riguardo a possibili nomi per i vostri futuri figli. :wink:

## Getting Started

Per utilizzare questo progetto � solo necessario collegarsi al sito ```http://piedino/cl_5ic/cl_5ic17/ajax/index.html``` ed entrare nella sezione autocomplete.

## Tecnologie utilizzate

### Per la parte front end:

Html5, css3, javascript vanilla, e bootstrap per amalgamare il tutto :heart:

### Per la parte back end:

php7 e mySql per la base di dati.

## Come usare l'applicativo:
Inserire le iniziali di nomi di persone all'interno della searchbar, per ottienere dei suggerimenti di possibili nomi! 
Magari in futuro si potr� anche fare qualcosa con quel suggerimento!

## Versioni

* ### versione 1.1 [stable]
    Come tutte le belle storie prima o poi finiscono :cry:

* ### versione 1.0 [beta]
    Prima versione finale, dovrebbe funzionare correttamente ma non si sa mai! 

* ### versione 0.3 [sperimentale]:
    Modifica alla parte front end e aggiunta di contenuti sul sito

* ### versione 0.2 [sperimentale]:
    Aggiunta la parte front end al software gi� presente

* ### versione 0.1 [sperimentale]:
    Prima versione di prova rilasciata


## Autori

programmata con :heart: da:

**Samuele Vanini** : *sviluppatore Back end.*

**Tommaso Garofolin e Alessandro Banciu** : *sviluppatore front end e studio sullo stile grafico.*

**Enrico Franzolin** : *scrittura delle informazioni presenti nel sito.*

**Sefano Rizzo** : *lavoro grafico e relatore di questo README e della wiki.*